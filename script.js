function createNewUser() {
    const firstName = prompt("Введите имя", "");
    const lastName = prompt("Введите фамилию", "");
    const birthday = prompt('Введите датй рождения в формате dd.mm.yyyy', "");

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getAge: function () {
            const day = this.birthday.substr(6, 4);
            const year = new Date().getFullYear();
            const result = +year - +day;
            return result;

        },
        getPassword: function() {
            const str = this.firstName.substr(0, 1);
            const name = str.toUpperCase() + this.lastName.toLowerCase();
            const  result = name + birthday;
            return result;
        },
        getLogin: function () {
            const str = this.firstName.substr(0, 1);
            const login = str + this.lastName;

            return login.toLowerCase();
        }
    };

    return newUser;
}

const user = createNewUser();

console.log(user.getPassword(), user.getAge());

